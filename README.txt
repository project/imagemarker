
-- SUMMARY --

For a full description of the module, visit the project page:
  http://drupal.org/project/imagemarker
  
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/imagemarker
  

-- CONTRIBUTERS --

swentel - http://drupal.org/user/107403
stalski - http://drupal.org/user/322618
jyve    - http://drupal.org/user/591438